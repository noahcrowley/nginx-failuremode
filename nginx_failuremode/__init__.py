import eventlet
eventlet.monkey_patch()

import os
import logging
import sh
import time
from flask import Flask, request, jsonify, render_template
from flasgger import Swagger
from flask_socketio import SocketIO, emit

FLASK_APP_DIR = os.path.dirname(os.path.abspath(__file__))

# Flask
app = Flask(__name__)
socketio = SocketIO(app)

Swagger(app)

TIMES = dict()
START_TIME = time.time()
TIME_LIST = list()

def config_app():
    """
    Loads environment parameters from application defaults.
    Required parameters:

    PLAN: A list of dictionaries containing the following keys:
      time: integer number of seconds
      response: the http response to be sent
    """
    #TODO add default config
    app.config.from_envvar('FAIL_CONFIG')
    global TIMES
    global TIME_LIST
    for item in app.config['PLAN']:
        start_time = int(time.time() + item['time'])
        TIMES[start_time] = item['response']
    TIME_LIST = sorted(TIMES.keys(), reverse=True)

config_app()

# Logging
logging.basicConfig(
    level=logging.DEBUG,
    format='[%(asctime)s] [%(name)s] [%(levelname)s] %(message)s',
    datefmt='%Y%m%d-%H:%M%p',)


@app.route('/')
def hello_world():
    """
    Returns information about the application.
    ---
    tags:
      - info
    """
    return 'A simple Flask app to test NGINX Failure Modes'


@app.route('/run_plan', methods=['GET'])
def run_plan():
    """
    The endpoint to use for running the test plan.
    Returns response codes according to the application configuration.
    ---
    tags:
      - failure testing
    """
    now_time = int(time.time())
    for t in enumerate(TIME_LIST):
        print("now_time: {}".format(now_time))
        print("t: {}".format(t))
        if (t[1] < now_time):
            break
    return jsonify({
        "the expected return code is" : TIMES[t[1]],
        "my_id" :  app.config['ID']
    }), TIMES[t[1]]


@app.route('/ws')
def websocket_index():
    """
    Returns an HTML page from which websocket connections can be made.
    ---
    tags:
      - websocket
    """
    return render_template('index.html')


@socketio.on('ping', namespace='/test')
def ws_ping(ping):
    print('ping: pong')
    emit('ping', 'pong')

@socketio.on('msg', namespace='/test')
def ws_message(msg):
    print(msg)